
import requests
from typing import Any

BASE_API = "https://api.prv.cymru/api/dictionary"

def get_info() -> dict[str, Any]:
    r = requests.get(BASE_API + "/info")
    return r.json()

def find_word_by_id(langugage_code: str, id: int) -> dict[str, Any]|None:
    requests.get(BASE_API)