

import commands.info
import commands.context
import interactions
import utils
from pathlib import Path
import traceback
import i18n
i18n.set('skip_locale_root_data', True)
i18n.set('filename_format', '{locale}.{format}')
i18n.set('fallback', 'en')
i18n.load_path.append('locale')


with open(Path("discord.key")) as key_file:
    client = interactions.Client(
        key_file.read().strip()
    )


async def exec_command(ctx: interactions.CommandContext, command, *args, **kwargs) -> None:
    await ctx.defer()
    context = commands.context.CommandContext(
        ctx,
        "nn"
    )
    try:
        embed = command(context, *args, **kwargs)
    except Exception as e:
        traceback.print_exc()
        embed = utils.create_exception(
            "Something went wrong, but I am not quite sure what! If this persist, please contact my author")
    await ctx.send(embeds=[embed])


@client.command(
    name="about",
    description="Prints some general information about the bot"
)
async def __about(ctx: interactions.CommandContext):
    await exec_command(ctx, commands.info.about)


@client.command(
    name="dictionaries",
    description="Prints some general information about the dictionaries"
)
async def __dictionaries(ctx: interactions.CommandContext):
    await exec_command(ctx, commands.info.dictionaries)

client.start()
