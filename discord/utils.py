
import interactions


def create_base_embed() -> interactions.Embed:
    return interactions.Embed(
        color=0x6452bc,
        author=interactions.EmbedAuthor(
            name="Digi",
            icon_url="https://opensourcedict.org/digi_v2.png"
        )
    )


def create_exception(error_message: str) -> interactions.Embed:
    return interactions.Embed(
        title="Ooopsie doopsie! Something went wrong!",
        color=0xff0000,
        author=interactions.EmbedAuthor(
            name="Digi",
            icon_url="https://opensourcedict.org/digi_v2.png"
        ),
        description=error_message
    )
