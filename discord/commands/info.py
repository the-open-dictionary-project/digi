
import interactions
import api
import languages
import utils
import i18n

from . import context


def about(ctx: context.CommandContext) -> interactions.Embed:
    api_data = api.get_info()

    codes = sorted(
        api_data['dictionaries'],
        key=lambda x: languages.lang_code_to_name(x, ctx.locale)
    )

    embed = utils.create_base_embed()
    embed.add_field(i18n.t("base.author", locale=ctx.locale), "Preben Vangberg", True)
    embed.add_field(
        i18n.t("linguistics.language", count=len(api_data['dictionaries']), locale=ctx.locale),
        len(api_data['dictionaries']),
        True
    )
    embed.add_field(i18n.t("base.license", locale=ctx.locale), "MIT", True)
    embed.add_field(
        i18n.t('base.repository', locale=ctx.locale),
        "https://gitlab.com/the-open-dictionary-project/digi"
    )

    return embed


def dictionaries(ctx: context.CommandContext) -> interactions.Embed:
    api_data = api.get_info()

    codes = sorted(
        api_data['dictionaries'],
        key=lambda x: languages.lang_code_to_name(x, ctx.locale)
    )

    embed = utils.create_base_embed()
    embed.description = "Here is a list of all of the dictionaries that are currently loaded by the API"

    for code in codes:
        words = api_data['dictionaries'][code]['numberOfLemmas']
        embed.add_field(
            languages.lang_code_to_name(code, ctx.locale),
            f"{i18n.t('linguistics.word', locale=ctx.locale, count=words)}: {words}"
        )
    return embed
