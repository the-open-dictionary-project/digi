
import dataclasses
import interactions


@dataclasses.dataclass
class CommandContext:
    interactions: interactions.CommandContext
    locale: str
