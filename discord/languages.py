
import i18n
import iso639

def lang_code_to_name(lang_code: str, locale: str ="en"):
    name = iso639.languages.get(alpha2=lang_code) \
        .name\
        .lower()\
        .replace(" ", "_")
    return i18n.t(f"languages.{name}", locale=locale)